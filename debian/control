Source: libpwizlite
Priority: optional
Maintainer: The Debichem Group <debichem-devel@lists.alioth.debian.org>
Uploaders: Filippo Rusconi <lopippo@debian.org>
Build-Depends: debhelper-compat (= 13),
 cmake,
 dh-exec,
 dpkg-dev (>= 1.18.25),
 libboost-dev,
 libboost-thread-dev,
 libboost-filesystem-dev,
 libboost-iostreams-dev,
 libboost-chrono-dev,
 zlib1g-dev,
 python3-docutils,
 libhdf5-dev
Standards-Version: 4.6.2
Section: libs
Homepage: https://salsa.debian.org/debichem-team/libpwizlite
Vcs-Browser: https://salsa.debian.org/debichem-team/libpwizlite
Vcs-Git: https://salsa.debian.org/debichem-team/libpwizlite.git


Package: libpwizlite-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends: libpwizlite3t64 (= ${binary:Version}),
 ${misc:Depends}
Replaces: libpwizlite-dev (<< ${binary:Version})
Description: Library to load mzML/mzXML files (dev files)
 This library is a dumbed-down version of the Proteowizard library.
 This library only contains the required features to
 load standard mzML/mzXML mass spectrometry data files.
 .
 See http://proteowizard.sourceforge.net/ for the original project.
 .
 This package ships the development files.


Package: libpwizlite3t64
Provides: ${t64:Provides}
Breaks: libpwizlite3 (<< ${source:Version})
Architecture: any
Multi-Arch: same
Depends: ${shlibs:Depends},
 ${misc:Depends}
Replaces: libpwizlite3, libpwizlite3t64 (<< ${binary:Version})
Description: Library to load mzML/mzXML files (runtime files)
 This library is a dumbed-down version of the Proteowizard library.
 This library only contains the required features to
 load standard mzML/mzXML mass spectrometry data files.
 .
 See http://proteowizard.sourceforge.net/ for the original project.
 .
 This package ships the runtime library files.

