# This file should be included if the command line reads like this:
# x86_64-w64-mingw32.shared-cmake -DCMAKE_BUILD_TYPE=Release -DMXE=1 ..

message("MXE (M cross environment) https://mxe.cc/")
message("Please run the configuration like this:")
message("x86_64-w64-mingw32.shared-cmake -DMXE=1 -G \"Unix Makefiles\" -DCMAKE_BUILD_TYPE=Release ../../development")

set(HOME_DEVEL_DIR "/home/rusconi/devel")

set(CMAKE_C_IMPLICIT_INCLUDE_DIRECTORIES ${HOME_DEVEL_DIR}/mxe/usr/x86_64-w64-mingw32.shared/include)
set(CMAKE_CXX_IMPLICIT_INCLUDE_DIRECTORIES ${HOME_DEVEL_DIR}/mxe/usr/x86_64-w64-mingw32.shared/include)

if(WIN32 OR _WIN32)
	message(STATUS "Building with WIN32 defined.")
endif()

message(STATUS "${BoldGreen}Setting definition -DPMSPP_LIBRARY for symbol DLL export.${ColourReset}")
add_definitions(-DPMSPP_LIBRARY)


find_package(ZLIB REQUIRED)


find_package(Boost COMPONENTS iostreams thread filesystem chrono REQUIRED)


#include(FindHDF5)
#find_package(HDF5 COMPONENTS CXX)
# Unfortunately, the libraries are not singled out correctly by
# the find_package call above.
# message(STATUS "HDF5_LIBRARIES: ${HDF5_LIBRARIES}")
# message(STATUS "HDF5_CXX_LIBRARIES: ${HDF5_CXX_LIBRARIES}")
# message(STATUS "HDF5_INCLUDE_DIRS: ${HDF5_INCLUDE_DIRS}")
# message(STATUS "HDF5_CXX_INCLUDE_DIRS: ${HDF5_CXX_INCLUDE_DIRS}")
# We need to provide the lib paths manually below.


message(STATUS "Add HDF5 libs as a MXE-specific link-time dependency.")
set(hdf5_FOUND 1)
set(hdf5_INCLUDE_DIRS "${HOME_DEVEL_DIR}/mxe/usr/x86_64-w64-mingw32.shared/include")

add_library(hdf5::generic UNKNOWN IMPORTED)
  set_target_properties(hdf5::generic PROPERTIES
    INTERFACE_INCLUDE_DIRECTORIES ${hdf5_INCLUDE_DIRS}
    IMPORTED_LOCATION "${HOME_DEVEL_DIR}/mxe/usr/x86_64-w64-mingw32.shared/bin/hdf5.dll")

add_library(hdf5::cxx UNKNOWN IMPORTED)
  set_target_properties(hdf5::cxx PROPERTIES
    INTERFACE_INCLUDE_DIRECTORIES ${hdf5_INCLUDE_DIRS}
    IMPORTED_LOCATION "${HOME_DEVEL_DIR}/mxe/usr/x86_64-w64-mingw32.shared/bin/hdf5_cpp.dll")

add_library(hdf5::hdf5_cpp INTERFACE IMPORTED)
set_property(TARGET hdf5::hdf5_cpp PROPERTY
  INTERFACE_LINK_LIBRARIES hdf5::generic hdf5::cxx)

#set(PLATFORM_SPECIFIC_LINK_LIBRARIES "${HOME_DEVEL_DIR}/mxe/usr/x86_64-w64-mingw32.shared/bin/hdf5_cpp.dll" "${HOME_DEVEL_DIR}/mxe/usr/x86_64-w64-mingw32.shared/bin/hdf5.dll")
