//
//  Copyright (c) 2012 Artyom Beilis (Tonkikh)
//
//  Distributed under the Boost Software License, Version 1.0. (See
//  accompanying file LICENSE_1_0.txt or copy at
//  http://www.boost.org/LICENSE_1_0.txt)
//
#ifndef BOOST_NOWIDE_CSTDLIB_HPP
#define BOOST_NOWIDE_CSTDLIB_HPP

#include <stdlib.h>
#include <errno.h>
#include <boost/nowide/stackstring.hpp>
namespace boost {
namespace nowide {

// Filippo Rusconi
// This code unit breaks the build in mingw64 with this message:

// [  1%] Building CXX object src/CMakeFiles/pwizlite-shared.dir/pwiz/utility/minimxml/XMLWriter.cpp.obj
// cd C:/msys64/home/mydar/devel/pwizlite/build-area/mingw64/src && C:/msys64/mingw64/bin/c++.exe  -DQT_NO_DEBUG_OUTPUT -DWITHOUT_MZ5 -Dpwizlite_shared_EXPORTS @CMakeFiles/pwizlite-shared.dir/includes_CXX.rsp -O3 -DNDEBUG   -Wno-unknown-pragmas -Wall -Wextra -Wall -std=gnu++17 -o CMakeFiles/pwizlite-shared.dir/pwiz/utility/minimxml/XMLWriter.cpp.obj -c C:/msys64/home/mydar/devel/pwizlite/development/src/pwiz/utility/minimxml/XMLWriter.cpp
// In file included from C:/msys64/home/mydar/devel/pwizlite/development/src/pwiz/utility/misc/Std.hpp:32,
//                  from C:/msys64/home/mydar/devel/pwizlite/development/src/pwiz/utility/minimxml/XMLWriter.cpp:26:
// C:/msys64/home/mydar/devel/pwizlite/development/src/pwiz/utility/misc/Stream.hpp:60:12: error: 'int boost::nowide::system(const char*)' conflicts with a previous declaration
//    60 | using bnw::system; // unqualified system() calls will be ambiguous, by intention, to force developers to consider UTF-8 compatibility
//       |            ^~~~~~
// In file included from C:/msys64/mingw64/include/c++/9.3.0/cstdlib:75,
//                  from C:/msys64/home/mydar/devel/pwizlite/development/src/pwiz/utility/misc/optimized_lexical_cast.hpp:27,
//                  from C:/msys64/home/mydar/devel/pwizlite/development/src/pwiz/utility/minimxml/XMLWriter.hpp:29,
//                  from C:/msys64/home/mydar/devel/pwizlite/development/src/pwiz/utility/minimxml/XMLWriter.cpp:25:
// C:/msys64/mingw64/x86_64-w64-mingw32/include/stdlib.h:519:15: note: previous declaration 'int system(const char*)'
//   519 |   int __cdecl system(const char *_Command);
//       |               ^~~~~~

// I thus try to check if not using this unit is ok.

using ::system;

// Not #if 0 out everything else

#if 0

#if !defined(BOOST_WINDOWS) && !defined(BOOST_NOWIDE_DOXYGEN)

using ::system;

#else // Windows

///
/// Same as std::system but cmd is UTF-8.
///
/// If the input is not valid UTF-8, -1 returned and errno set to EINVAL
///
inline int system(char const *cmd)
{
    if(!cmd)
        return _wsystem(0);
    wstackstring wcmd;
    if(!wcmd.convert(cmd)) {
        errno = EINVAL;
        return -1;
    }
    return _wsystem(wcmd.c_str());
}

#endif

#endif // 0

} // nowide
} // namespace boost

#endif
///
// vim: tabstop=4 expandtab shiftwidth=4 softtabstop=4
